package com.laubrass.poc.ext;

import com.laubrass.poc.annotation.CoreAnnotation;
import com.laubrass.poc.model.CoreInterfaceOne;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * Created by pturcotte on 17/02/15.
 */
@Configuration
@ComponentScan(basePackages = {"com.laubrass.poc"})
@EnableAutoConfiguration
public class ExtensionApplication {

    public static void main(String... args) {
        SpringApplication.run(ExtensionApplication.class, args);
    }

    @Bean
    @Order(2)
    public CoreInterfaceOne extensionBean() {
        return new CoreInterfaceOne() {
            @Override
            public String getName() {
                return "extensionBean from ExtensionApplication order 2";
            }

            @Override
            public Boolean isActive() {
                return null;
            }
        };
    }

    @Bean
    @CoreAnnotation(value = "internal")
    public Object extensionObject() {
        return new Object();
    }
}
