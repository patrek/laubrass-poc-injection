package com.laubrass.poc.runner;

import com.laubrass.poc.model.CoreInterfaceOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by pturcotte on 17/02/15.
 */
@Component
public class CoreRunner implements CommandLineRunner {

    @Autowired
    List<CoreInterfaceOne> interfaceOnes;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("In the CoreRunner");
        runFromInterface();
    }

    private void runFromInterface() {
        if (null != interfaceOnes) {
            System.out.println("Found " + interfaceOnes.size() + " bean with interface " + CoreInterfaceOne.class.getName());
            for (CoreInterfaceOne interfaceOne : interfaceOnes) {
                System.out.println("interface : " + interfaceOne.getName());
            }
        } else {
            System.out.println("THere is no list of interfaceOne");
        }
    }
}
