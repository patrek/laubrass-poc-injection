package com.laubrass.poc.annotation;

import java.lang.annotation.*;

/**
 * Created by pturcotte on 17/02/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
@Documented
public @interface CoreAnnotation {

    String value() default "";
    String name() default "";
}
