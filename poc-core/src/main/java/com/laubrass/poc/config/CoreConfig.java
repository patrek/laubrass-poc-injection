package com.laubrass.poc.config;

import com.laubrass.poc.model.CoreInterfaceOne;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pturcotte on 17/02/15.
 */
@Configuration
public class CoreConfig {

    @Bean
    public CoreInterfaceOne configInterfaceOne() {
        return new CoreInterfaceOne() {
            @Override
            public String getName() {
                return "configInterfaceOne bean from CoreConfig";
            }

            @Override
            public Boolean isActive() {
                return Boolean.FALSE;
            }
        };

    }
}
