package com.laubrass.poc;

import com.laubrass.poc.model.CoreInterfaceOne;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * Created by pturcotte on 17/02/15.
 */
@Configuration
@ComponentScan()
@EnableAutoConfiguration
public class CoreApplication {

    public static void main(String... args) {
        System.out.println("Core Application Starting");
        SpringApplication.run(CoreApplication.class, args);
        System.out.println("Core Application running");
    }

    @Bean
    @Order(1)
    public CoreInterfaceOne initialBean() {
        System.out.println("Creating initialBean");
        return new CoreInterfaceOne() {
            @Override
            public String getName() {
                return "initialBean with order 1";
            }

            @Override
            public Boolean isActive() {
                return Boolean.TRUE;
            }
        };
    }

}
